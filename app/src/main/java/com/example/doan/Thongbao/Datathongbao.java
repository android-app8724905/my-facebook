package com.example.doan.Thongbao;

public class Datathongbao {
    private String id_user_thongbao;
    private int kieuthongbao;
    private String time_thongbao;
    private String use_id_baiviet;

    public Datathongbao() {
    }

    public Datathongbao(String id_user_thongbao, int kieuthongbao, String time_thongbao) {
        this.id_user_thongbao = id_user_thongbao;
        this.kieuthongbao = kieuthongbao;
        this.time_thongbao = time_thongbao;
    }

    public String getId_user_thongbao() {
        return id_user_thongbao;
    }

    public void setId_user_thongbao(String id_user_thongbao) {
        this.id_user_thongbao = id_user_thongbao;
    }

    public int getKieuthongbao() {
        return kieuthongbao;
    }

    public void setKieuthongbao(int kieuthongbao) {
        this.kieuthongbao = kieuthongbao;
    }

    public String getTime_thongbao() {
        return time_thongbao;
    }

    public void setTime_thongbao(String time_thongbao) {
        this.time_thongbao = time_thongbao;
    }

    public String getUse_id_baiviet() {
        return use_id_baiviet;
    }

    public void setUse_id_baiviet(String use_id_baiviet) {
        this.use_id_baiviet = use_id_baiviet;
    }

    @Override
    public String toString() {
        return "Datathongbao{" +
                "id_user_thongbao='" + id_user_thongbao + '\'' +
                ", kieuthongbao=" + kieuthongbao +
                ", time_thongbao='" + time_thongbao + '\'' +
                ", use_id_baiviet='" + use_id_baiviet + '\'' +
                '}';
    }
}

package com.example.doan.Thongbao;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doan.AddFriends_ChapNhan.ChapnhanketbanFragment;
import com.example.doan.R;
import com.example.doan.base.BaseFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class ThongBaoFragment extends BaseFragment {

    private RecyclerView rvThongBao;
    private ThongBaoAdapter thongBaoAdapter;
    private Calendar calendar = Calendar.getInstance();

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_thong_bao;
    }

    @Override
    protected void anhXa() {
        super.anhXa();

        rvThongBao = requireView().findViewById(R.id.rvThongBao);
    }

    @Override
    protected void thietLapView() {
        super.thietLapView();
        ArrayList<Datathongbao> arrayList = new ArrayList<>();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef_like = database.getReference().child("Thong Bao").child(user.getUid()).child("1");
        DatabaseReference myRef_binhluan = database.getReference().child("Thong Bao").child(user.getUid()).child("2");
        DatabaseReference myRef_addfriends = database.getReference().child("Thong Bao").child(user.getUid()).child("3");
        rvThongBao.setAdapter(thongBaoAdapter);
        myRef_like.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Datathongbao datathongbao = dataSnapshot.getValue(Datathongbao.class);
                    arrayList.add(0,datathongbao);
                }
                thongBaoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), "loi r", Toast.LENGTH_SHORT).show();
            }
        });
        myRef_binhluan.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Datathongbao datathongbao = dataSnapshot.getValue(Datathongbao.class);
                    arrayList.add(0,datathongbao);
                }
                thongBaoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), "loi r", Toast.LENGTH_SHORT).show();
            }
        });
        myRef_addfriends.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Datathongbao datathongbao = dataSnapshot.getValue(Datathongbao.class);
                    arrayList.add(0,datathongbao);
                }
                thongBaoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), "loi r", Toast.LENGTH_SHORT).show();
            }
        });

        thongBaoAdapter = new ThongBaoAdapter(arrayList, getActivity(), new langnghethongbao() {
            @Override
            public void clickitem_addfriends(Boolean item) {
                loadFragment(new ChapnhanketbanFragment());
            }

            @Override
            public void clickitem_binhluan(Boolean item) {

            }

            @Override
            public void clickitem_like(Boolean item) {

            }

        });
        rvThongBao.setAdapter(thongBaoAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        rvThongBao.addItemDecoration(dividerItemDecoration);
    }
    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentController, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

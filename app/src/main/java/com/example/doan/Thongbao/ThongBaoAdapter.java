package com.example.doan.Thongbao;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doan.R;
import com.example.doan.comment.Giaodien_comment;
import com.example.doan.datafirebase.User_Taikhoan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ThongBaoAdapter extends RecyclerView.Adapter<ThongBaoAdapter.Viewholder>{

    private ArrayList<Datathongbao> datathongbaoArrayList;
    Context context;
    private langnghethongbao langnghethongbao;

    public ThongBaoAdapter(ArrayList<Datathongbao> datathongbaoArrayList, Context context, com.example.doan.Thongbao.langnghethongbao langnghethongbao) {
        this.datathongbaoArrayList = datathongbaoArrayList;
        this.context = context;
        this.langnghethongbao = langnghethongbao;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_thongbao,parent,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Datathongbao user = datathongbaoArrayList.get(position);
        if (user == null){
            return;
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Tai Khoan").child(user.getId_user_thongbao());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                Glide.with(holder.imgAvt)
                        .load(user_taikhoan.getAnhdaidien())
                        .into(holder.imgAvt);
                if(user.getKieuthongbao() == 1){
                    holder.tv_thongbao.setText(user_taikhoan.getName()+" đã thích bài viết của bạn");
                    Glide.with(holder.imgLogo)
                            .load(R.drawable.ic_logo_like)
                            .into(holder.imgLogo);
                   // Thongbao(1,user_taikhoan.getName());
                }
                if (user.getKieuthongbao()==2){
                    holder.tv_thongbao.setText(user_taikhoan.getName()+" đã bình luận bài viết của bạn");
                    Glide.with(holder.imgLogo)
                            .load(R.drawable.ic_logo_comment)
                            .into(holder.imgLogo);
                    //Thongbao(2,user_taikhoan.getName());
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, Giaodien_comment.class);
                            intent.putExtra("id_baiviet",user.getUse_id_baiviet());
                            intent.putExtra("id_use_baiviet",user.getUse_id_baiviet());
                            context.startActivity(intent);
                        }
                    });
                }
                if (user.getKieuthongbao()==3){
                    holder.tv_thongbao.setText(user_taikhoan.getName()+" đã gửi lời mới kết bạn với bạn");
                    Glide.with(holder.imgLogo)
                            .load(R.drawable.ic_logo_addfriend)
                            .into(holder.imgLogo);
                    //Thongbao(3,user_taikhoan.getName());
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            langnghethongbao.clickitem_addfriends(true);
                        }
                    });
                }


                holder.tv_time.setText(Diffdate(user.getTime_thongbao()));

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        holder.tv_thongbao.setText(datathongbaoArrayList.get(position).getTime_thongbao());
    }

    @Override
    public int getItemCount() {
        return datathongbaoArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        TextView tv_thongbao,tv_time;
        ImageView imgAvt,imgLogo;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            tv_thongbao = (TextView) itemView.findViewById(R.id.tv_thongbao);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            imgAvt = (ImageView) itemView.findViewById(R.id.img_avt);
            imgLogo = (ImageView) itemView.findViewById(R.id.img_logo);

        }
    }
    public static String Diffdate(String str) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat _newformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date datetime = _newformat.parse( str);
            String currentime = _newformat.format(c.getTime());
            Date _cvcurrent = _newformat.parse(currentime);
            Long diff = _cvcurrent.getTime() - datetime.getTime();
            // trừ ngày tháng năm tính thời gian cập nhật
            int hours = (int) (diff / (1000 * 60 * 60));
            int mins = (int) (diff / (1000 * 60)) % 60;
            int days = (int) (diff / (24 * 60 * 60 * 1000));

            if (days > 0) {
                return days + " ngày trước";
            } else {
                if (hours > 0) {
                    return hours + " giờ trước";
                } else {
                    return mins + " phút trước";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "Vừa Xong";
        }
    }
//    private void Thongbao(int style, String name){
//        String title = "";
//        Bitmap bitmap = BitmapFactory.decodeResource(Resources.getSystem(),R.drawable.facebooklogo);
//        if(style == 1){
//            title = name + " đã thích bài viết của bạn";
//        }if(style == 2){
//            title = name + " đã bình luận bài viết của bạn";
//        }if(style == 3){
//            title = name + " đã gửi lời mới kết bạn với bạn";
//        }
//        Notification notification = new Notification.Builder(context)
//                .setContentTitle("Facebook_face")
//                .setContentText(title)
//                .setSmallIcon(R.drawable.facebooklogo)
//                .setLargeIcon(bitmap)
//                .build();
//        NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//    }
}

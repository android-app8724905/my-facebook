package com.example.doan.main;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.doan.Giaodienchinh.TrangChuFragment;
import com.example.doan.R;
import com.example.doan.Thongbao.ThongBaoFragment;
import com.example.doan.Trangcanhan.TrangcanhanFragment;
import com.example.doan.base.BaseActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class TrangChinhActivity extends BaseActivity {

    private BottomNavigationView bottomNavigationView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_trang_chinh;
    }

    @Override
    protected void anhXa() {
        super.anhXa();

        bottomNavigationView = findViewById(R.id.navigation);
    }

    @Override
    protected void thietLapView() {
        super.thietLapView();

        /**
         * Hiển thị fragment trang chủ mỗi lần activiyt dc hiển thị
         */
        loadFragment(new TrangChuFragment());
    }

    @Override
    protected void hanhDong() {
        super.hanhDong();

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.navigationTrangChu:
                                loadFragment(new TrangChuFragment());
                                break;
                            case R.id.navigationBanBe:
                                loadFragment(new BanBeFragment());
                                break;
                            case R.id.navigationThongBao:
                                loadFragment(new ThongBaoFragment());
                                break;
                            case R.id.navigationTrangCaNhan:
                                    loadFragment(new TrangcanhanFragment());
                                break;

                        }
                        return true;
                    }
                }
        );
    }

    /**
     * Hiện fragment được truyền vào [R.id.fragmentController]
     */
    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentController, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}

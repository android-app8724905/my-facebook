package com.example.doan.main;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.doan.AddFriends_ChapNhan.ChapnhanketbanFragment;
import com.example.doan.AddFriends_GoiY.LoimoiketbanFragment;
import com.example.doan.R;
import com.example.doan.base.BaseFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BanBeFragment extends BaseFragment {
    private BottomNavigationView bottomNavigationView;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_ban_be;
    }

    @Override
    protected void anhXa() {
        super.anhXa();

        bottomNavigationView = requireView().findViewById(R.id.navigation_babe);
    }

    @Override
    protected void thietLapView() {
        super.thietLapView();
        loadFragment(new LoimoiketbanFragment());
    }

    @Override
    protected void hanhDong() {
        super.hanhDong();
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_chapnhan:
                                loadFragment(new ChapnhanketbanFragment());
                                break;
                            case R.id.item_loimoiketban:
                                loadFragment(new LoimoiketbanFragment());
                                break;

                        }
                        return true;
                    }
                }
        );
    }
    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentController_babe, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

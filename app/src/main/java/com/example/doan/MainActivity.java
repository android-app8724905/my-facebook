package com.example.doan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.doan.QuenMatKhau.NhapSDT;
import com.example.doan.main.TrangChinhActivity;
import com.example.doan.taotaikhoan.Ten;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private Button btnDangNhap, btnTaotaikhoan,btnQuenmatkhau;
    private EditText edt_taikhoan, edt_matkhau;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Anhxa();

        btnTaotaikhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Ten.class);
                startActivity(intent);
            }
        });
        btnQuenmatkhau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NhapSDT.class);
                startActivity(intent);
            }
        });
        btnDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                check();

            }
        });
    }
    private void Anhxa(){
        btnDangNhap = (Button) findViewById(R.id.btndangnhap);
        btnQuenmatkhau = (Button) findViewById(R.id.btnquenmatkhau);
        btnTaotaikhoan = (Button) findViewById(R.id.btntaotaikhoan);
        edt_taikhoan = (EditText) findViewById(R.id.edttaikhoan);
        edt_matkhau = (EditText) findViewById(R.id.edtmatkhau);
        progressDialog = new ProgressDialog(this);
    }
    private void check(){
        String taikhoan, matkhau;
        taikhoan = edt_taikhoan.getText().toString().trim();
        matkhau = edt_matkhau.getText().toString().trim();
        mAuth.signInWithEmailAndPassword(taikhoan, matkhau)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            progressDialog.dismiss();
                            Intent intent = new Intent(MainActivity.this, TrangChinhActivity.class);
                            startActivity(intent);
                        }
                        else {
                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Tai Khoan Hoac Mat Khau Khong Chinh Xac", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}
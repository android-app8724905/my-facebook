package com.example.doan.Trangcanhan;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.example.doan.Giaodienchinh.TrangChuFragment;
import com.example.doan.MainActivity;
import com.example.doan.R;
import com.example.doan.Thongbao.ThongBaoFragment;
import com.example.doan.baiviet.Them_bai_viet_moi;
import com.example.doan.base.BaseFragment;
import com.example.doan.datafirebase.User_Taikhoan;
import com.example.doan.main.BanBeFragment;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

public class TrangcanhanFragment extends BaseFragment {

    Button btntrangchu,btnloimoikb,btndanhsachbb,btnthongbao,btndangxuat,btnthembaiviet;
    ImageView img_avt_trangcanhan, img_anhbia,img_camera_avt,img_camera_anhbia;
    TextView tv_ten,tv_email;
    private Uri mUrl,mUrl2, mUrl_data, mUrl_data2;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReference().child("Anh dai dien");
    StorageReference storageRef2 = storage.getReference().child("Anh bia");
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Tai Khoan").child(user.getUid());




    @Override
    protected int getLayoutId() {
        return R.layout.fragment_trang_ca_nhan;
    }


    @Override
    protected void anhXa() {
        super.anhXa();
        img_camera_avt = requireView().findViewById(R.id.camera_anhdaidien);
        img_camera_anhbia = requireView().findViewById(R.id.camera_anhbia);
        btntrangchu = requireView().findViewById(R.id.btn_Trang_chu);
        btnloimoikb = requireView().findViewById(R.id.btn_loi_moi_kb);
        btndanhsachbb = requireView().findViewById(R.id.btn_quan_li_bai_viet);
        btnthongbao = requireView().findViewById(R.id.btn_thong_bao);
        btndangxuat = requireView().findViewById(R.id.btn_dang_xuat);
        btnthembaiviet = requireView().findViewById(R.id.btn_Them_bai_viet_moi);
        img_anhbia = requireView().findViewById(R.id.img_anhbia);
        img_avt_trangcanhan = requireView().findViewById(R.id.img_Anhdaidien_trangcanhan);
        tv_email = requireView().findViewById(R.id.tv_email);
        tv_ten = requireView().findViewById(R.id.tv_Ten);
        loadData();

    }

    @Override
    protected void hanhDong() {

        super.hanhDong();



        btntrangchu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragment(new TrangChuFragment());

            }
        });
        btnloimoikb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragment(new BanBeFragment());
            }
        });
        btndanhsachbb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragment2(new Trangcanhan_baivietFragment(),user.getUid());
            }
        });
        btnthongbao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragment(new ThongBaoFragment());
            }
        });
        btnthembaiviet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Them_bai_viet_moi.class);
                startActivity(intent);
            }
        });
        btndangxuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }


        });
        img_camera_avt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickphoto();
            }
        });
        img_camera_anhbia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickphoto2();
            }
        });
    }


    private void pickphoto(){
        Intent pickPhoto = new Intent();
        pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
        pickPhoto.setType("image/*");
        startActivityForResult(pickPhoto,1);
    }
    private void pickphoto2(){
        Intent pickPhoto = new Intent();
        pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
        pickPhoto.setType("image/*");
        startActivityForResult(pickPhoto,2);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1 :
                if (resultCode == Activity.RESULT_OK) {

                    mUrl = data.getData();
                    img_avt_trangcanhan.setImageURI(mUrl);
                    saveanhdaidien();

                }
            case 2:
                if (resultCode == Activity.RESULT_OK ){

                    mUrl2 = data.getData();
                    img_anhbia.setImageURI(mUrl2);
                    saveanhbia();


                }
        }
    }



    private void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }
    private void loadData(){
        if (user != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("Tai Khoan").child(user.getUid());
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);

                    String email = user.getEmail();


                    tv_ten.setText(user_taikhoan.getName());
                    tv_email.setText(email);
                    Glide.with(getActivity()).load(user_taikhoan.getAnhdaidien()).into(img_avt_trangcanhan);

                    Glide.with(img_anhbia).load(user_taikhoan.getAnhbia()).into(img_anhbia);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

    }


    private void saveanhdaidien(){
        Calendar calendar = Calendar.getInstance();
        StorageReference mountainsRef = storageRef.child("img" + calendar.getTimeInMillis()+".png");
        // Get the data from an ImageView as bytes
        img_avt_trangcanhan.setDrawingCacheEnabled(true);
        img_avt_trangcanhan.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) img_avt_trangcanhan.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });
        uploadTask = storageRef.putFile(mUrl);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return storageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    mUrl_data = downloadUri;
                    updateanhdaidien();
                } else {
                }
            }
        });


    }


    private void saveanhbia(){
        Calendar calendar = Calendar.getInstance();
        StorageReference mountainsRef = storageRef2.child("img" + calendar.getTimeInMillis()+".png");
        // Get the data from an ImageView as bytes
        img_anhbia.setDrawingCacheEnabled(true);
        img_anhbia.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) img_anhbia.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });
        uploadTask = storageRef2.putFile(mUrl2);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return storageRef2.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    mUrl_data2 = downloadUri;
                    updateanhbia();
                } else {
                }
            }
        });


    }

    private void updateanhdaidien(){
        String link;
        link = String.valueOf(mUrl_data);
        myRef.child("anhdaidien").setValue(link, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
            }
        });
    }
    private void updateanhbia(){
        String link;
        link = String.valueOf(mUrl_data2);
        myRef.child("anhbia").setValue(link, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
            }
        });
    }



    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentController, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void loadFragment2(Fragment fragment, String id_use) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle a = new Bundle();
        a.putString("id_use",id_use);
        fragment.setArguments(a);
        transaction.replace(R.id.fragmentController, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }




}

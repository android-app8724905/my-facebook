package com.example.doan.Trangcanhan;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doan.R;
import com.example.doan.base.BaseFragment;
import com.example.doan.datafirebase.User_Taikhoan;
import com.example.doan.datafirebase.User_baiviet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Trangcanhan_baivietFragment extends BaseFragment {

    private String id_use;
    private ImageView img_avt,img_anhbia;
    private TextView tv_ten;
    private RecyclerView recyclerView;
    private TrangCanhan_baivietAdapter trangCanhanBaivietAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_trang_nhan_baiviet;
    }

    @Override
    protected void anhXa() {
        super.anhXa();
        Bundle args = getArguments();
        id_use = args.getString("id_use");
        img_avt = requireView().findViewById(R.id.img_Anhdaidien_trangcanhan);
        img_anhbia = requireView().findViewById(R.id.img_anhbia_trangcanhan);
        tv_ten = requireView().findViewById(R.id.tv_Ten);

        loadData();
    }

    private void loadData(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Tai Khoan").child(id_use);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                tv_ten.setText(user_taikhoan.getName());
                Glide.with(getActivity()).load(user_taikhoan.getAnhdaidien()).into(img_avt);
                Glide.with(img_anhbia).load(user_taikhoan.getAnhbia()).into(img_anhbia);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    protected void thietLapView() {
        super.thietLapView();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Bai Viet");
        recyclerView = requireView().findViewById(R.id.recylerview_trangcanhan);
        ArrayList<User_baiviet> arrayList = new ArrayList<>();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (arrayList != null){
                    arrayList.clear();
                }
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    User_baiviet user_baiviet = dataSnapshot.getValue(User_baiviet.class);
                    if(user_baiviet.getId_user().contains(id_use)){
                        arrayList.add(0,user_baiviet);
                    }

                }
                trangCanhanBaivietAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), "loi r", Toast.LENGTH_SHORT).show();
            }
        });



        trangCanhanBaivietAdapter = new TrangCanhan_baivietAdapter(arrayList, getActivity());
        recyclerView.setAdapter(trangCanhanBaivietAdapter);

    }

}

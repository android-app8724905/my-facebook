package com.example.doan.Trangcanhan;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doan.R;
import com.example.doan.comment.Giaodien_comment;
import com.example.doan.datafirebase.Data_comment;
import com.example.doan.datafirebase.Data_luotlike;
import com.example.doan.datafirebase.User_Taikhoan;
import com.example.doan.datafirebase.User_baiviet;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TrangCanhan_baivietAdapter extends RecyclerView.Adapter<TrangCanhan_baivietAdapter.ViewHolder> {
    private ArrayList<User_baiviet> dataArrayList;
    Context context;
    private Calendar calendar = Calendar.getInstance();

    public TrangCanhan_baivietAdapter(ArrayList<User_baiviet> dataArrayList, Context context) {
        this.dataArrayList = dataArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemview = layoutInflater.inflate(R.layout.item_baiviet,parent,false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User_baiviet user = dataArrayList.get(position);
        if(user == null){
            return;
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user_taikhoan = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef2 = database.getReference("Bai Viet").child(user.getId_baiviet());
        DatabaseReference myRef_cmt = database.getReference("Bai Viet").child(user.getId_baiviet()).child("Binh Luan");
        ArrayList<Data_luotlike> luotlikeArrayList = new ArrayList<>();
        myRef2.child("luot_like").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Data_luotlike data_luotlike = dataSnapshot.getValue(Data_luotlike.class);
                    luotlikeArrayList.add(0,data_luotlike);
                    if (data_luotlike.getId_use().equals(user_taikhoan.getUid())){
                        holder.tvlike.setHintTextColor(context.getResources().getColor(R.color.fb));
                        holder.tvlike.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                holder.tvlike.setHintTextColor(context.getResources().getColor(R.color.black));
                                myRef2.child("luot_like").child(data_luotlike.getId_use()).removeValue();
                            }
                        });

                    }

                }
                String a = String.valueOf(luotlikeArrayList.size());
                holder.tvluotthich.setText(a+" luot thich");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        ArrayList<Data_comment> commentArrayList = new ArrayList<>();
        myRef_cmt.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Data_comment data_comment = dataSnapshot.getValue(Data_comment.class);
                    commentArrayList.add(0,data_comment);
                    String b = String.valueOf(commentArrayList.size());
                    holder.tvbinhluan.setText(b + " binh luan");

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        holder.tvmota.setText(user.getMota());
        holder.tvlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.tvlike.setHintTextColor(context.getResources().getColor(R.color.fb));
                Data_luotlike data_luotlike = new Data_luotlike(user_taikhoan.getUid(),String.valueOf(calendar.getTimeInMillis()));
                myRef2.child("luot_like").child(user_taikhoan.getUid()).setValue(data_luotlike, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                        if(error == null){
                            Toast.makeText(context, "da like bai viet", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        holder.tvcmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Giaodien_comment.class);
                intent.putExtra("id_baiviet",user.getId_baiviet());
                context.startActivity(intent);

            }
        });

        DatabaseReference myRef = database.getReference("Tai Khoan").child(user.getId_user());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                holder.txtTen.setText(user_taikhoan.getName());
                Glide.with(holder.imgavt)
                        .load(user_taikhoan.getAnhdaidien())
                        .into(holder.imgavt);
                holder.tvtime.setText(Diffdate(user.getTime_post()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(context, "loi up ten", Toast.LENGTH_SHORT).show();

            }
        });
        Glide.with(holder.imganh)
                .load(user.getHinhanh())
                .into(holder.imganh);

    }


    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTen,tvluotthich,tvbinhluan,tvmota,tvtime,tvlike,tvcmt;
        ImageView imgavt,imganh;
        public ViewHolder(@NonNull View itemView) {
                super(itemView);
                txtTen = (TextView) itemView.findViewById(R.id.tvTen);
                tvluotthich = (TextView) itemView.findViewById(R.id.tvluotthich);
                tvbinhluan = (TextView) itemView.findViewById(R.id.tvbinhluan);
                imgavt = (ImageView) itemView.findViewById(R.id.imgavt);
                imganh = (ImageView) itemView.findViewById(R.id.imgAnh);
                tvmota = (TextView) itemView.findViewById(R.id.tvmota);
                tvlike = (TextView) itemView.findViewById(R.id.img_like);
                tvcmt = (TextView) itemView.findViewById(R.id.img_cmt);
                tvtime = (TextView) itemView.findViewById(R.id.tv_time_baiviet);
        }
    }

    public static String Diffdate(String str) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat _newformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date datetime = _newformat.parse( str);
            String currentime = _newformat.format(c.getTime());
            Date _cvcurrent = _newformat.parse(currentime);
            Long diff = _cvcurrent.getTime() - datetime.getTime();
            // trừ ngày tháng năm tính thời gian cập nhật
            int hours = (int) (diff / (1000 * 60 * 60));
            int mins = (int) (diff / (1000 * 60)) % 60;
            int days = (int) (diff / (24 * 60 * 60 * 1000));

            if (days > 0) {
                return days + " ngày trước";
            } else {
                if (hours > 0) {
                    return hours + " giờ trước";
                } else {
                    return mins + " phút trước";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "Vừa Xong";
        }
    }
}

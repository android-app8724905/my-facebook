package com.example.doan.datafirebase;

public class Data_luotlike {
    private String id_use;
    private String time_like;

    public Data_luotlike() {
    }

    public Data_luotlike(String id_use, String time_like) {
        this.id_use = id_use;
        this.time_like = time_like;
    }

    public String getId_use() {
        return id_use;
    }

    public void setId_use(String id_use) {
        this.id_use = id_use;
    }

    public String getTime_like() {
        return time_like;
    }

    public void setTime_like(String time_like) {
        this.time_like = time_like;
    }

    @Override
    public String toString() {
        return "Data_luotlike{" +
                "id_use='" + id_use + '\'' +
                ", time_like='" + time_like + '\'' +
                '}';
    }
}

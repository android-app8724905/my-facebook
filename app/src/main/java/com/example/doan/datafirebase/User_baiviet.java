package com.example.doan.datafirebase;

public class User_baiviet {
    private String mota;
    private String hinhanh;
    private String id_user;
    private String time_post;
    private int so_luotlike;
    private int so_luotcmt;
    private String id_baiviet;

    public User_baiviet() {
    }

    public User_baiviet(String mota, String hinhanh, String id_user, String time_post, int so_luotlike, int so_luotcmt, String id_baiviet) {
        this.mota = mota;
        this.hinhanh = hinhanh;
        this.id_user = id_user;
        this.time_post = time_post;
        this.so_luotlike = so_luotlike;
        this.so_luotcmt = so_luotcmt;
        this.id_baiviet = id_baiviet;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTime_post() {
        return time_post;
    }

    public void setTime_post(String time_post) {
        this.time_post = time_post;
    }

    public int getSo_luotlike() {
        return so_luotlike;
    }

    public void setSo_luotlike(int so_luotlike) {
        this.so_luotlike = so_luotlike;
    }

    public int getSo_luotcmt() {
        return so_luotcmt;
    }

    public void setSo_luotcmt(int so_luotcmt) {
        this.so_luotcmt = so_luotcmt;
    }

    public String getId_baiviet() {
        return id_baiviet;
    }

    public void setId_baiviet(String id_baiviet) {
        this.id_baiviet = id_baiviet;
    }

    @Override
    public String toString() {
        return "User_baiviet{" +
                "mota='" + mota + '\'' +
                ", hinhanh='" + hinhanh + '\'' +
                ", id_user='" + id_user + '\'' +
                ", time_post='" + time_post + '\'' +
                ", so_luotlike=" + so_luotlike +
                ", so_luotcmt=" + so_luotcmt +
                ", id_baiviet='" + id_baiviet + '\'' +
                '}';
    }
}

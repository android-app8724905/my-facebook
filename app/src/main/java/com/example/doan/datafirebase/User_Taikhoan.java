package com.example.doan.datafirebase;

public class User_Taikhoan {
    private String id_user;
    private String name;
    private String email;
    private String sdt;
    private String anhdaidien;
    private String anhbia;

    public User_Taikhoan() {
    }

    public User_Taikhoan(String id_user, String name, String email, String sdt, String anhdaidien, String anhbia) {
        this.id_user = id_user;
        this.name = name;
        this.email = email;
        this.sdt = sdt;
        this.anhdaidien = anhdaidien;
        this.anhbia = anhbia;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getAnhdaidien() {
        return anhdaidien;
    }

    public void setAnhdaidien(String anhdaidien) {
        this.anhdaidien = anhdaidien;
    }

    public String getAnhbia() {
        return anhbia;
    }

    public void setAnhbia(String anhbia) {
        this.anhbia = anhbia;
    }

    @Override
    public String toString() {
        return "User_Taikhoan{" +
                "id_user='" + id_user + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", sdt='" + sdt + '\'' +
                ", anhdaidien='" + anhdaidien + '\'' +
                ", anhbia='" + anhbia + '\'' +
                '}';
    }
}

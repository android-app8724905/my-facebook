package com.example.doan.datafirebase;

public class Data_comment {
    private String id_user_binhluan;
    private String tvbinhluan;
    private String time_cmt;

    public Data_comment() {
    }

    public Data_comment(String id_user_binhluan, String tvbinhluan, String time_cmt) {
        this.id_user_binhluan = id_user_binhluan;
        this.tvbinhluan = tvbinhluan;
        this.time_cmt = time_cmt;
    }

    public String getId_user_binhluan() {
        return id_user_binhluan;
    }

    public void setId_user_binhluan(String id_user_binhluan) {
        this.id_user_binhluan = id_user_binhluan;
    }

    public String getTvbinhluan() {
        return tvbinhluan;
    }

    public void setTvbinhluan(String tvbinhluan) {
        this.tvbinhluan = tvbinhluan;
    }

    public String getTime_cmt() {
        return time_cmt;
    }

    public void setTime_cmt(String time_cmt) {
        this.time_cmt = time_cmt;
    }

    @Override
    public String toString() {
        return "Data_comment{" +
                "id_user_binhluan='" + id_user_binhluan + '\'' +
                ", tvbinhluan='" + tvbinhluan + '\'' +
                ", time_cmt='" + time_cmt + '\'' +
                '}';
    }
}

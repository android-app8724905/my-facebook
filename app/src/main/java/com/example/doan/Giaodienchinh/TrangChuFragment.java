package com.example.doan.Giaodienchinh;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doan.R;
import com.example.doan.Trangcanhan.Trangcanhan_baivietFragment;
import com.example.doan.base.BaseFragment;
import com.example.doan.datafirebase.User_baiviet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TrangChuFragment extends BaseFragment {

    private RecyclerView rvBaiViet;
    private GiaodienAdapter giaodienAdapter;
    private langnghe langnghe;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_trang_chu;
    }

    @Override
    protected void anhXa() {
        super.anhXa();

        rvBaiViet = requireView().findViewById(R.id.rvBaiViet);
    }
    @Override
    protected void thietLapView() {
        super.thietLapView();
        DatabaseReference myRef = database.getReference().child("Bai Viet");

        RecyclerView recyclerView = (RecyclerView) requireView().findViewById(R.id.rvBaiViet);
        ArrayList<User_baiviet> arrayList = new ArrayList<>();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (arrayList != null){
                    arrayList.clear();
                }
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    User_baiviet user_baiviet = dataSnapshot.getValue(User_baiviet.class);
                    arrayList.add(0,user_baiviet);
                }
                giaodienAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getActivity(), "loi r", Toast.LENGTH_SHORT).show();
            }
        });



        giaodienAdapter = new GiaodienAdapter(arrayList, getActivity(), new langnghe() {
            @Override
            public void cliklavt(Boolean avt, String id_use) {
                loadFragment(new Trangcanhan_baivietFragment(),id_use);

            }
        });
        recyclerView.setAdapter(giaodienAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getContext().getResources().getDrawable(R.drawable.custom_duonglienket));
        recyclerView.addItemDecoration(dividerItemDecoration);

    }
    private void loadFragment(Fragment fragment, String id_use) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle a = new Bundle();
        a.putString("id_use",id_use);
        fragment.setArguments(a);
        transaction.replace(R.id.fragmentController, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }


}

package com.example.doan.Giaodienchinh;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.doan.MainActivity;
import com.example.doan.R;
import com.example.doan.main.TrangChinhActivity;
import com.google.firebase.auth.FirebaseAuth;

public class StartActivyti extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_activyti);
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null){
            Intent intent = new Intent(StartActivyti.this, TrangChinhActivity.class);
            startActivity(intent);
            progressDialog.dismiss();
        }
        else {
            Intent intent = new Intent(StartActivyti.this, MainActivity.class);
            startActivity(intent);
            progressDialog.dismiss();
        }


    }
}
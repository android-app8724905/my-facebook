package com.example.doan.BanBe;

public class Data_BanBe {
    String id_user;

    public Data_BanBe() {
    }

    public Data_BanBe(String id_user) {
        this.id_user = id_user;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    @Override
    public String toString() {
        return "Data_BanBe{" +
                "id_user='" + id_user + '\'' +
                '}';
    }
}

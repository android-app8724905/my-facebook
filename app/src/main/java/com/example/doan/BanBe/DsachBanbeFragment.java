package com.example.doan.BanBe;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doan.R;
import com.example.doan.base.BaseFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DsachBanbeFragment extends BaseFragment {
    private RecyclerView recyclerView_dsachbanbe;
    private EditText edt_dsachbanbe;
    private ImageView img_search_dsachbanbe;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_dsachbanbe;
    }

    @Override
    protected void anhXa() {
        super.anhXa();
        recyclerView_dsachbanbe = requireView().findViewById(R.id.rv_dsachbanbe);
        edt_dsachbanbe = requireView().findViewById(R.id.edt_search_dsachbanbe);
        img_search_dsachbanbe = requireView().findViewById(R.id.img_search_dsachbanbe);
    }

    @Override
    protected void hanhDong() {
        super.hanhDong();
        img_search_dsachbanbe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    protected void thietLapView() {
        super.thietLapView();
        loaddulieu();
    }

    private void loaddulieu(){
        String key;
        key = edt_dsachbanbe.getText().toString().trim();
        RecyclerView recyclerView = (RecyclerView) requireView().findViewById(R.id.rv_dsachbanbe);
        ArrayList<Data_BanBe> arrayList = new ArrayList<>();
        DsachAdapter dsachAdapter = new DsachAdapter(arrayList, getActivity());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef = database.getReference().child("Tai Khoan").child(user.getUid()).child("ban be");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (arrayList != null){
                    arrayList.clear();
                }
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Data_BanBe data_banBe = dataSnapshot.getValue(Data_BanBe.class);
                    arrayList.add(data_banBe);
                }
                dsachAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
        recyclerView.setAdapter(dsachAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);


    }
}

package com.example.doan.BanBe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doan.R;
import com.example.doan.datafirebase.User_Taikhoan;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DsachAdapter extends RecyclerView.Adapter<DsachAdapter.ViewHolder>{
    private ArrayList<Data_BanBe> data_banBeArrayList;
    Context context;

    public DsachAdapter(ArrayList<Data_BanBe> data_banBeArrayList, Context context) {
        this.data_banBeArrayList = data_banBeArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_dsachbanbe,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Data_BanBe user_danhsach = data_banBeArrayList.get(position);
        if (user_danhsach == null){
            return;
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef = database.getReference("Tai Khoan").child(user_danhsach.getId_user());
        DatabaseReference myRef2 = database.getReference("Tai Khoan").child(user.getUid());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                String id = user_danhsach.getId_user();
                Toast.makeText(context, id, Toast.LENGTH_SHORT).show();
                holder.tvTenbb.setText(user_taikhoan.getName());
                Glide.with(holder.imgavtbb)
                        .load(user_taikhoan.getAnhdaidien())
                        .into(holder.imgavtbb);

                holder.btn_xoa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, id, Toast.LENGTH_SHORT).show();
                        myRef2.child("ban be").child(id).removeValue();
                        Toast.makeText(context, "Da xoa thanh cong", Toast.LENGTH_SHORT).show();


                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(context, "loi up ten", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return data_banBeArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgavtbb;
        TextView tvTenbb,tvLoimoikb;
        Button btn_xoa;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgavtbb = (ImageView) itemView.findViewById(R.id.imgavtbb_danhsach);
            tvTenbb = (TextView) itemView.findViewById(R.id.tvtenbanbe_danhsach);
            tvLoimoikb = (TextView) itemView.findViewById(R.id.tvsobanchung_danhsach);
            btn_xoa = (Button) itemView.findViewById(R.id.btn_delete_babe);

        }
    }
}

package com.example.doan.taotaikhoan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.doan.R;
import com.example.doan.datafirebase.User_Taikhoan;
import com.example.doan.main.TrangChinhActivity;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

public class thongtincanhan extends AppCompatActivity {


    private Button btn_update,btn_thoat;
    private EditText edt_name,edt_email,edt_phone;
    private ImageView img_avt_update;
    private Uri mUrl,mUrl_data;
    private ProgressDialog progressDialog;


    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference().child("Anh dai dien");
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference("Tai Khoan").child(user.getUid());
    private DatabaseReference myRef_danhsachtaikhoan = database.getReference("Danh sach tai khoan");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_ca_nhan);
        anhxa();
        updatedata();
        hanhdong();
    }
    private void anhxa(){
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_phone = (EditText) findViewById(R.id.edt_sdt);
        btn_update = (Button) findViewById(R.id.btn_update);
        btn_thoat =(Button) findViewById(R.id.btn_thoat);
        img_avt_update = (ImageView) findViewById(R.id.img_avt_update);
        progressDialog = new ProgressDialog(this);
    }

    private void hanhdong(){
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUrl == null){
                    Toast.makeText(thongtincanhan.this, "Chưa cập nhật ảnh", Toast.LENGTH_SHORT).show();
                }
                else{
                    progressDialog.show();
                    saveanh();
                }

            }
        });
        btn_thoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(thongtincanhan.this,Ten.class);
                startActivity(intent);
            }
        });
        img_avt_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickphoto();
            }
        });
    }


    private void update() {
        String name,phone,email,anhdaidien,anhbia,id_user;
        name = edt_name.getText().toString().trim();
        phone = edt_phone.getText().toString().trim();
        email = edt_email.getText().toString().trim();
        anhdaidien = String.valueOf(mUrl_data);
        anhbia = "";
        id_user="";
        User_Taikhoan user_taikhoan = new User_Taikhoan(id_user,name,email,phone,anhdaidien,anhbia);
        myRef.setValue(user_taikhoan, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                myRef.child("id_user").setValue(ref.getKey());
                progressDialog.dismiss();
                Intent intent = new Intent(thongtincanhan.this, TrangChinhActivity.class);
                startActivity(intent);
            }
        });

    }


    private void updatedata(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if(user == null){
            return;
        }
        else {
            edt_email.setText(user.getEmail());
            Glide.with(this).load(user.getPhotoUrl()).error(R.drawable.ic_img).into(img_avt_update);
        }
    }
    private void pickphoto(){
        Intent pickPhoto = new Intent();
        pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
        pickPhoto.setType("image/*");
        startActivityForResult(pickPhoto,1);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            mUrl = data.getData();
            Glide.with(this)
                    .load(mUrl)
                    .into(img_avt_update);
        }
    }

    private void saveanh(){
        Calendar calendar = Calendar.getInstance();
        StorageReference mountainsRef = storageRef.child("img" + calendar.getTimeInMillis()+".png");
        // Get the data from an ImageView as bytes
        img_avt_update.setDrawingCacheEnabled(true);
        img_avt_update.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) img_avt_update.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(thongtincanhan.this, "Up hinh that bai", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });
        uploadTask = storageRef.putFile(mUrl);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return storageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    mUrl_data = downloadUri;
                    update();
                } else {
                }
            }
        });


    }


}





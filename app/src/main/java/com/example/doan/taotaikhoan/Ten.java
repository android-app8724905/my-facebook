package com.example.doan.taotaikhoan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.doan.MainActivity;
import com.example.doan.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Ten extends AppCompatActivity {

    private Button btnDangki;
    private EditText edtemail,edtpasswok;
    private ProgressDialog progressDialog;
    private TextView tv_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_ky);
        Anhxa();
        btnDangki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                truyendata();

            }
        });
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Ten.this, MainActivity.class);
                startActivity(i);
            }
        });

    }
    private void Anhxa(){
        btnDangki = (Button) findViewById(R.id.btn_dang_ky);
        edtemail = (EditText)findViewById(R.id.edt_email);
        tv_login = (TextView)findViewById(R.id.tv_login);
        edtpasswok = (EditText) findViewById(R.id.edt_matkhau);
        progressDialog = new ProgressDialog(this);



    }
    private void truyendata(){
        String email,password;
        email = edtemail.getText().toString().trim();
        password = edtpasswok.getText().toString().trim();

        progressDialog.show();

        if(email.isEmpty() || password.isEmpty() ){
            Toast.makeText(this, "Bạn nhập chưa đủ thông tin", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
            return;
        }    
        if(password.length()<6){
            progressDialog.dismiss();
            Toast.makeText(this, "password phai lon hon 6", Toast.LENGTH_SHORT).show();
        }

        else {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            if (task.isSuccessful()) {
                                Intent i = new Intent(Ten.this, thongtincanhan.class);
                                startActivity(i);
                                finishAffinity();
                            } else {
                                Toast.makeText(Ten.this, "that bai", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }

    }
}

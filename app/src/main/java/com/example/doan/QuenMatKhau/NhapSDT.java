package com.example.doan.QuenMatKhau;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.doan.MainActivity;
import com.example.doan.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class NhapSDT extends AppCompatActivity {
    private Button btnSDT;
    private EditText edt_email_check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_sdt);
        Anhxa();
        btnSDT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kiemtra();
            }
        });
    }

    private void kiemtra() {
        
        if(edt_email_check.getText().toString().isEmpty()){
            Toast.makeText(this, "Bạn chưa nhập sđt", Toast.LENGTH_SHORT).show();
        }
        else {
            String email = edt_email_check.getText().toString().trim();
            FirebaseAuth auth = FirebaseAuth.getInstance();
            String emailAddress = email;

            auth.sendPasswordResetEmail(emailAddress)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(NhapSDT.this, "Vui lòng kiểm tra email của bạn", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(NhapSDT.this, MainActivity.class);
                                startActivity(i);
                            }
                            else {
                                Intent intent = new Intent(NhapSDT.this,Chuacotaikhoan.class);
                                startActivity(intent);
                            }
                        }
                    });
        }
        
    }

    private void Anhxa(){
        btnSDT = (Button) findViewById(R.id.btn_check_sdt);
        edt_email_check = (EditText) findViewById(R.id.edt_nhap_email);
    }
}
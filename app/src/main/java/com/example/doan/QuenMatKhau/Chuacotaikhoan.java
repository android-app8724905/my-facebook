package com.example.doan.QuenMatKhau;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.doan.R;
import com.example.doan.taotaikhoan.Ten;

public class Chuacotaikhoan extends AppCompatActivity {
    private Button btnTaoTK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khong_tim_thay_tai_khoan);
        Anhxa();
        btnTaoTK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Chuacotaikhoan.this, Ten.class);
                startActivity(intent);
            }
        });
    }
    private void Anhxa(){
        btnTaoTK = (Button) findViewById(R.id.btn_tao_tk_news);
    }
}
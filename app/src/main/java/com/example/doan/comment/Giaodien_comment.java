package com.example.doan.comment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doan.R;
import com.example.doan.Thongbao.Datathongbao;
import com.example.doan.datafirebase.Data_comment;
import com.example.doan.main.TrangChinhActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class Giaodien_comment extends AppCompatActivity {

    private RecyclerView recyclerView;
    private EditText edt_binhluan_moi;
    private ImageView img_gui_binhluan,img_avt_binhluan,img_back;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef_cmt = database.getReference().child("Bai Viet");
    private String id_baiviet,id_use_baiviet;
    private ProgressDialog progressDialog;
    private Calendar calendar = Calendar.getInstance();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Bundle b = getIntent().getExtras();
        id_baiviet = b.getString("id_baiviet");
        id_use_baiviet = b.getString("id_use_baiviet");
        anhxa();
        loadbinhluan();
        binhluan();
        hanhdong();


    }
    private void loadbinhluan(){

    }
    private void hanhdong(){
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Giaodien_comment.this, TrangChinhActivity.class);
                startActivity(intent);
            }
        });

        img_gui_binhluan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                luudata();
            }
        });

    }
    private void luudata(){
        FirebaseUser user_taikhoan = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef_thongbao = database.getReference("Thong Bao").child(id_use_baiviet);
        String mbinhluan;
        SimpleDateFormat _newformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        String currentime = _newformat.format(c.getTime());
        mbinhluan = edt_binhluan_moi.getText().toString().trim();
        Data_comment data_comment = new Data_comment(user.getUid(),mbinhluan,currentime);
        myRef_cmt.child(id_baiviet).child("Binh Luan").push().setValue(data_comment, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                if (error == null){
                    Toast.makeText(Giaodien_comment.this, "Binh luan cua ban da duoc dang tai", Toast.LENGTH_SHORT).show();
                    edt_binhluan_moi.setText("");
                }
            }
        });
        Datathongbao datathongbao = new Datathongbao(user_taikhoan.getUid(),
                2,currentime);
        myRef_thongbao.child("2").child(user_taikhoan.getUid()).setValue(datathongbao, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {

            }
        });
        myRef_thongbao.child("2").child(user_taikhoan.getUid()).child("use_id_baiviet").setValue(id_baiviet);



    }
    private void anhxa(){
        edt_binhluan_moi = (EditText) findViewById(R.id.edt_binhluan_news);
        img_avt_binhluan = (ImageView)findViewById(R.id.img_avt_binhluan_news);
        img_gui_binhluan = (ImageView)findViewById(R.id.img_guibinhluan);
        img_back = (ImageView)findViewById(R.id.img_back_cmt);
        progressDialog = new ProgressDialog(this);
    }
    private void binhluan(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Bai Viet").child(id_baiviet).child("Binh Luan");
        recyclerView = (RecyclerView)findViewById(R.id.recylerview_comment);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        ArrayList<Data_comment> arrayList = new ArrayList<>();
        CommentAdapter commentAdapter = new CommentAdapter(arrayList,getApplicationContext());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (arrayList != null){
                    arrayList.clear();
                }
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Data_comment data_comment = dataSnapshot.getValue(Data_comment.class);
                    arrayList.add(data_comment);
                }
                commentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(Giaodien_comment.this, "loi r", Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.setAdapter(commentAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(Giaodien_comment.this,DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

}
package com.example.doan.comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doan.R;
import com.example.doan.datafirebase.Data_comment;
import com.example.doan.datafirebase.User_Taikhoan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CommentAdapter  extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{

    private ArrayList<Data_comment> data_commentArrayList;
    Context context;

    public CommentAdapter(ArrayList<Data_comment> data_commentArrayList, Context context) {
        this.data_commentArrayList = data_commentArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_comment,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Data_comment user = data_commentArrayList.get(position);
        if (user == null){
            return;
        }
        holder.tvcomment.setText(user.getTvbinhluan());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Tai Khoan").child(user.getId_user_binhluan());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                holder.tv_ten_user_binhluan.setText(user_taikhoan.getName());
                Glide.with(holder.imghinhanh)
                        .load(user_taikhoan.getAnhdaidien())
                        .into(holder.imghinhanh);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(context, "loi up ten", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return data_commentArrayList.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imghinhanh;
        TextView tvcomment,tv_ten_user_binhluan;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imghinhanh = (ImageView) itemView.findViewById(R.id.img_hinh_comment);
            tvcomment = (TextView) itemView.findViewById(R.id.tvcomment);
            tv_ten_user_binhluan = (TextView) itemView.findViewById(R.id.tv_ten_user_binhluan);
        }
    }
}

package com.example.doan.AddFriends_ChapNhan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doan.BanBe.Data_BanBe;
import com.example.doan.R;
import com.example.doan.datafirebase.User_Taikhoan;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class BanBe_chapnhanAdapter extends RecyclerView.Adapter<BanBe_chapnhanAdapter.ViewHolder> {
    private ArrayList<Data_addfriends_chapnhan> data_addfriends_chapnhans;
    Context context;

    public BanBe_chapnhanAdapter(ArrayList<Data_addfriends_chapnhan> data_addfriends_chapnhans, Context context) {
        this.data_addfriends_chapnhans = data_addfriends_chapnhans;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_chapnhan_ketban,parent,false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull BanBe_chapnhanAdapter.ViewHolder holder, int position) {
        Data_addfriends_chapnhan user_chapnhan = data_addfriends_chapnhans.get(position);
        if (user_chapnhan == null){
            return;
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef = database.getReference("Tai Khoan").child(user_chapnhan.getId_user());
        DatabaseReference myRef2 = database.getReference("Tai Khoan").child(user.getUid());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                String id = user_chapnhan.getId_user();
                holder.tvTenbb.setText(user_taikhoan.getName());
                Glide.with(holder.imgavtbb)
                        .load(user_taikhoan.getAnhdaidien())
                        .into(holder.imgavtbb);

                holder.btn_xoa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, id, Toast.LENGTH_SHORT).show();
                        myRef2.child("loi moi ket ban").child(id).removeValue();
                        Toast.makeText(context, "Da xoa thanh cong", Toast.LENGTH_SHORT).show();


                    }
                });
                holder.btn_xacnhan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Data_BanBe data_banBe = new Data_BanBe(id);
                        myRef2.child("ban be").child(id).setValue(data_banBe);
                        myRef2.child("loi moi ket ban").child(id).removeValue();

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(context, "loi up ten", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return data_addfriends_chapnhans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgavtbb;
        TextView tvTenbb,tvLoimoikb;
        Button btn_xacnhan,btn_xoa;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgavtbb = (ImageView) itemView.findViewById(R.id.imgavtbb);
            tvTenbb = (TextView) itemView.findViewById(R.id.tvtenbanbe1);
            tvLoimoikb = (TextView) itemView.findViewById(R.id.tvsobanchung);
            btn_xacnhan = (Button) itemView.findViewById(R.id.btnchapnhan);
            btn_xoa = (Button) itemView.findViewById(R.id.btnxoa);

        }
    }
}

package com.example.doan.AddFriends_ChapNhan;

public class Data_addfriends_chapnhan {
    private String id_user;

    public Data_addfriends_chapnhan() {
    }

    public Data_addfriends_chapnhan(String id_user) {
        this.id_user = id_user;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    @Override
    public String toString() {
        return "Data_addfriends_chapnhan{" +
                "id_user='" + id_user + '\'' +
                '}';
    }
}

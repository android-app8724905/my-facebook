package com.example.doan.AddFriends_ChapNhan;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doan.R;
import com.example.doan.base.BaseFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChapnhanketbanFragment extends BaseFragment {
    private RecyclerView rvBanBe_chapnhan;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_chapnhan_ketban;
    }
    @Override
    protected void anhXa() {
        super.anhXa();

        rvBanBe_chapnhan = requireView().findViewById(R.id.rv_chapnhan_ketban);
    }
    @Override
    protected void thietLapView() {
        super.thietLapView();
        RecyclerView recyclerView = (RecyclerView) requireView().findViewById(R.id.rv_chapnhan_ketban);
        ArrayList<Data_addfriends_chapnhan> arrayList = new ArrayList<>();
        BanBe_chapnhanAdapter banBeAdapter = new BanBe_chapnhanAdapter(arrayList, getActivity());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef = database.getReference().child("Tai Khoan").child(user.getUid()).child("loi moi ket ban");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (arrayList != null){
                    arrayList.clear();
                }
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Data_addfriends_chapnhan data_addfriends_chapnhan = dataSnapshot.getValue(Data_addfriends_chapnhan.class);
                    arrayList.add(data_addfriends_chapnhan);
                }
                banBeAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
        recyclerView.setAdapter(banBeAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

}

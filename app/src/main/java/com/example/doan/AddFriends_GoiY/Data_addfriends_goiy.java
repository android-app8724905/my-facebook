package com.example.doan.AddFriends_GoiY;

public class Data_addfriends_goiy {
    private String id_user;

    public Data_addfriends_goiy() {
    }

    public Data_addfriends_goiy(String id_user) {
        this.id_user = id_user;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    @Override
    public String toString() {
        return "Data_addfriends_goiy{" +
                "id_user='" + id_user + '\'' +
                '}';
    }
}

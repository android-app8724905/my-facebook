package com.example.doan.AddFriends_GoiY;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doan.R;
import com.example.doan.base.BaseFragment;
import com.example.doan.datafirebase.User_Taikhoan;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class LoimoiketbanFragment extends BaseFragment {

    private BanBe_goiyAdapter banBe_goiyAdapter;
    private EditText edtSearch;
    private ImageView imgSearch;
    private RecyclerView rvBanBe;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_danh_sach_ban_be;
    }
    @Override
    protected void anhXa() {
        super.anhXa();

        rvBanBe = requireView().findViewById(R.id.rvBanBe);
        edtSearch = requireView().findViewById(R.id.edt_search);
        imgSearch = requireView().findViewById(R.id.img_search);
    }
    @Override
    protected void thietLapView() {
        super.thietLapView();
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaddata();
                edtSearch.setText("");
            }
        });

    }
    private void loaddata(){
        RecyclerView recyclerView = (RecyclerView) requireView().findViewById(R.id.rvBanBe);
        ArrayList<Data_addfriends_goiy> arrayList = new ArrayList<>();
        BanBe_goiyAdapter banBe_goiyAdapter = new BanBe_goiyAdapter(arrayList, getActivity());
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Tai Khoan");
        String key = edtSearch.getText().toString().trim();
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                    if (user_taikhoan != null) {
                        if(user_taikhoan.getName().contains(key))
                        {
                            Data_addfriends_goiy data_addfriends_goiy = new Data_addfriends_goiy(user_taikhoan.getId_user());
                            arrayList.add(data_addfriends_goiy);
                        }
                    }
                banBe_goiyAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        recyclerView.setAdapter(banBe_goiyAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

}

package com.example.doan.AddFriends_GoiY;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doan.AddFriends_ChapNhan.Data_addfriends_chapnhan;
import com.example.doan.R;
import com.example.doan.Thongbao.Datathongbao;
import com.example.doan.datafirebase.User_Taikhoan;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class BanBe_goiyAdapter extends RecyclerView.Adapter<BanBe_goiyAdapter.ViewHolder>{

    private ArrayList<Data_addfriends_goiy> databanbeArrayList;
    Context context;
    private Calendar calendar = Calendar.getInstance();

    public BanBe_goiyAdapter(ArrayList<Data_addfriends_goiy> databanbeArrayList, Context context) {
        this.databanbeArrayList = databanbeArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_ketban,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Data_addfriends_goiy user = databanbeArrayList.get(position);
        FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null){
            return;
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Tai Khoan").child(user.getId_user());
        if (user.getId_user().equals(user1.getUid())){
            holder.btn_add.setVisibility(View.GONE);
        }
        myRef.child("loi moi ket ban").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String loimoiketban_string = snapshot.toString();
                if (loimoiketban_string.contains(user1.getUid())){
                    holder.btn_add.setText("Huỷ");
                    holder.btn_add.setTextColor(ContextCompat.getColor(context,R.color.black));
                    holder.btn_add.setBackgroundResource(R.color.xam);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        myRef.child("ban be").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String banbe_string = snapshot.toString();
                if(banbe_string.contains(user1.getUid())){
                    holder.btn_add.setText("Bạn Bè");
                    holder.btn_add.setTextColor(ContextCompat.getColor(context,R.color.black));
                    holder.btn_add.setBackgroundResource(R.color.xam);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User_Taikhoan user_taikhoan = snapshot.getValue(User_Taikhoan.class);
                holder.tvTenbb1.setText(user_taikhoan.getName());
                Glide.with(holder.imgavtbb1)
                        .load(user_taikhoan.getAnhdaidien())
                        .into(holder.imgavtbb1);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(context, "loi up ten", Toast.LENGTH_SHORT).show();

            }
        });
        holder.btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, user1.getUid(), Toast.LENGTH_SHORT).show();
                Toast.makeText(context, user.getId_user(), Toast.LENGTH_SHORT).show();
                holder.btn_add.setText("Huỷ");
                holder.btn_add.setTextColor(ContextCompat.getColor(context,R.color.black));
                holder.btn_add.setBackgroundResource(R.color.xam);
                FirebaseUser user_taikhoan = FirebaseAuth.getInstance().getCurrentUser();
                DatabaseReference myRef_thongbao = database.getReference("Thong Bao").child(user.getId_user());
                SimpleDateFormat _newformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                Calendar c = Calendar.getInstance();
                String currentime = _newformat.format(c.getTime());
                Data_addfriends_chapnhan data_addfriends_chapnhan = new Data_addfriends_chapnhan(user1.getUid());
                myRef.child("loi moi ket ban").child(user1.getUid()).setValue(data_addfriends_chapnhan);
                Datathongbao datathongbao = new Datathongbao(user_taikhoan.getUid(),
                        3,currentime);
                myRef_thongbao.child("3").child(user_taikhoan.getUid()).setValue(datathongbao, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {

                    }
                });
            }
        });


    }

    @Override
    public int getItemCount() {
        return databanbeArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgavtbb1;
        TextView tvTenbb1,tvLoimoikb1;
        Button btn_add;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgavtbb1 = (ImageView) itemView.findViewById(R.id.imgavtbb);
            tvTenbb1 = (TextView) itemView.findViewById(R.id.tvtenbanbe1);
            tvLoimoikb1 = (TextView) itemView.findViewById(R.id.tvsobanchung);
            btn_add = (Button) itemView.findViewById(R.id.btn_add_banbe);

        }
    }
}

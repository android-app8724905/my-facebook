package com.example.doan.baiviet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.doan.R;
import com.example.doan.datafirebase.User_baiviet;
import com.example.doan.main.TrangChinhActivity;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Them_bai_viet_moi extends AppCompatActivity {

    private ImageView imgBack, imgHinhanh;
    private Button btn_dang_bai;
    private EditText edt_mota;
    private Uri mUrl,mUrl_data;
    private ProgressDialog progressDialog;

    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference().child("Anh bai viet");
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference("Bai Viet");
    private DatabaseReference myRef_cmt = database.getReference("Binh Luan");
    private Calendar calendar = Calendar.getInstance();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_bai_viet_moi);
        Anhxa();
        HanhDong();

    }
    private void Anhxa(){
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgHinhanh = (ImageView)findViewById(R.id.imgAnh_news);
        btn_dang_bai = (Button) findViewById(R.id.btn_dang_bai);
        edt_mota = (EditText) findViewById(R.id.edt_mota);
        progressDialog = new ProgressDialog(this);
    }
    private void HanhDong(){

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Them_bai_viet_moi.this, TrangChinhActivity.class);
                startActivity(intent);
            }
        });

        imgHinhanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickphoto();
            }
        });
        btn_dang_bai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                saveanh();
            }
        });

    }

    private void pickphoto(){
        Intent pickPhoto = new Intent();
        pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
        pickPhoto.setType("image/*");
        startActivityForResult(pickPhoto,1);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if(resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                mUrl = selectedImage;
                imgHinhanh.setImageURI(selectedImage);
            }

    }

    private void update(){
        String Mota,hinhanh,time,id_baiviet;
        int so_luotlike = 0,so_luotcmt = 0;
        Mota = edt_mota.getText().toString().trim();
        hinhanh = String.valueOf(mUrl_data);
        SimpleDateFormat _newformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Calendar c = Calendar.getInstance();
        String currentime = _newformat.format(c.getTime());
        id_baiviet = "";
        User_baiviet user_baiviet =  new User_baiviet(Mota,hinhanh,user.getUid(),currentime,so_luotlike,so_luotcmt,id_baiviet);
        myRef.push().setValue(user_baiviet, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                if(error == null) {
                    myRef.child(ref.getKey()).child("id_baiviet").setValue(ref.getKey());
                    progressDialog.dismiss();
                    Intent intent = new Intent(Them_bai_viet_moi.this, TrangChinhActivity.class);
                    startActivity(intent);
                    Toast.makeText(Them_bai_viet_moi.this, "Bài Viết Đã Được Đăng", Toast.LENGTH_SHORT).show();
                    myRef_cmt.setValue(ref.getKey());
                }
            }
        });


    }
    private void saveanh(){

        StorageReference mountainsRef = storageRef.child("img" + calendar.getTimeInMillis()+".png");
        // Get the data from an ImageView as bytes
        imgHinhanh.setDrawingCacheEnabled(true);
        imgHinhanh.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) imgHinhanh.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(Them_bai_viet_moi.this, "Up hinh that bai", Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });
        uploadTask = storageRef.putFile(mUrl);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return storageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    mUrl_data = downloadUri;
                    update();
                } else {
                }
            }
        });

    }





    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentController, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }




}